#!/bin/bash

# Configuration
REPO="fossas/fossa-cli"
GITHUB_API_URL="https://api.github.com/repos/$REPO/releases/latest"
ARTIFACTORY_URL="https://louphub.jfrog.io/artifactory/fossa-generic-local/releases"
ARTIFACTORY_ACCESS_TOKEN="$ARTIFACTORY_ACCESS_TOKEN"
TEAMS_WEBHOOK_URL=${TEAMS_WEBHOOK_URL}
TEMP_DIR="./temp_assets"

# Fetch the latest release data from GitHub API
release_data=$(curl -s $GITHUB_API_URL)

# Verify successful fetch from GitHub
if [ $? -ne 0 ]; then
    echo "Error fetching release data from GitHub."
    exit 1
fi

# Extract the latest version number
LATEST_VERSION=$(echo "$release_data" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/')

# Check if the release already exists in Artifactory
ARTIFACTORY_RESPONSE=$(curl -s -o /dev/null -w "%{http_code}" -H "X-JFrog-Art-Api:${ARTIFACTORY_ACCESS_TOKEN}" "${ARTIFACTORY_URL}/$LATEST_VERSION/")

# Proceed if the release is not in Artifactory
if [ "$ARTIFACTORY_RESPONSE" -eq 200 ]; then
    echo "Release ${LATEST_VERSION} already exists in Artifactory. No action taken."
    exit 0
else
    echo "Release ${LATEST_VERSION} does not exist in Artifactory. Proceeding with download and upload."
fi

# Extract asset URLs and source code archive URLs
ASSET_URLS=$(echo "$release_data" | grep '"browser_download_url":' | sed -E 's/.*"([^"]+)".*/\1/')
SOURCE_CODE_ZIP_URL=$(echo "$release_data" | grep '"zipball_url":' | sed -E 's/.*"([^"]+)".*/\1/')
SOURCE_CODE_TAR_URL=$(echo "$release_data" | grep '"tarball_url":' | sed -E 's/.*"([^"]+)".*/\1/')

# Create a temporary directory to store downloaded assets
mkdir -p "$TEMP_DIR"
echo "Processing assets for $LATEST_VERSION..."

# Download and upload assets
for asset_url in $ASSET_URLS $SOURCE_CODE_ZIP_URL $SOURCE_CODE_TAR_URL; do
    asset_filename=$(basename "$asset_url")
    
    # Download the asset or source code archive
    echo "Downloading $asset_filename..."
    curl -sL "$asset_url" -o "$TEMP_DIR/$asset_filename"
    
    # Check for curl error
    if [ $? -ne 0 ]; then
        echo "Error downloading $asset_filename."
        continue  # Skip uploading this file if download failed
    fi

    # Upload the file to Artifactory
    echo "Uploading $asset_filename to Artifactory..."
    full_artifactory_path="${ARTIFACTORY_URL}/$LATEST_VERSION/$asset_filename"
    response=$(curl -w "%{http_code}" -o response.txt -H "X-JFrog-Art-Api:${ARTIFACTORY_ACCESS_TOKEN}" -T "$TEMP_DIR/$asset_filename" "$full_artifactory_path")
    
    # Check response code for success (200 OK or 201 Created)
    if [[ "$response" -ne 200 ]] && [[ "$response" -ne 201 ]]; then
        echo "Error uploading $asset_filename. Response code: $response"
        cat response.txt  # Output response content for debugging
    else
        echo "Uploaded $asset_filename successfully."
    fi
done

# Clean up the temporary directory
rm -rf "$TEMP_DIR"
echo "All assets for $LATEST_VERSION have been processed and uploaded to Artifactory."
